# gnome-shell-extension-arcmenu

[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://img.shields.io/badge/License-GPL%20v2-blue.svg)

Application menu for GNOME Shell, designed to provide a more traditional user experience and workflow.

https://gitlab.com/arcmenu/ArcMenu/

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/extensions/gnome-shell-extension-arcmenu.git
```
